package com.softserve.edu.app;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseUtils {

    /**
     *
     * @param filename File name to search in
     * @param s string to be found
     * @return number of s occurrences in file f
     */
    public static int calculateStringInFile(String filename, String s) throws IOException {

        int counter = 0;
        Pattern p = Pattern.compile(s);

        List<String> lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);

        for(String line: lines) {
            Matcher m = p.matcher(line);
            while(m.find()) {
                counter++;
            }
        }

        return counter;

    }

    public static void findAndReplace(String filename, String s, String newStr) throws IOException {

        Pattern p = Pattern.compile(s);

        List<String> lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);

        for(int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            Matcher m = p.matcher(line);

            lines.set(i, m.replaceAll(newStr));

        }

        Files.write(Paths.get(filename), lines, StandardOpenOption.TRUNCATE_EXISTING);

    }

}
