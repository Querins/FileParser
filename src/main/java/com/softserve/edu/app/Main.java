package com.softserve.edu.app;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        switch(args.length) {

            case 0:
                printInstructions();
                return;
            case 1:
                System.out.println("One argument is not enough.");
                printInstructions();
                return;
            case 2:

                String fileName = args[0];
                String subStr = args[1];
                try {
                    int res = ParseUtils.calculateStringInFile(fileName, subStr);
                    System.out.format("Result is %d", res);
                } catch (IOException e) {
                    System.out.println(e.getLocalizedMessage());
                }
                break;
            case 3:

                fileName = args[0];
                subStr = args[1];
                String newStr = args[2];

                try {

                    ParseUtils.findAndReplace(fileName, subStr, newStr);
                    System.out.println("Parsed successfully");

                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                break;
            default:

        }

    }

    private static void printInstructions() {
        System.out.println("========== File parser ===========");
        System.out.println("This program has 2 working modes: ");
        System.out.println("When 2 arguments specified, searches for string occurrences in the file specified by the 1st argument");
        System.out.println("string to search for is specified by 2nd argument");
        System.out.println("When 3 arguments are given, replaces any string provided by the second argument to one provided by the 3rd");
        System.out.println("Meaning of the first argument is identical in both modes");
        System.out.println("Any additional arguments will be ignored");
    }

}
